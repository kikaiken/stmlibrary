# PSController Library

## Overview

PlayStationのController及びそれに準じたコントローラ(ヴィストンのCS-V3等)の
ボタン・スティック情報を読むライブラリ
<5ms の間隔ごとに値を読むことができる

## CubeMX setting

### SPIの設定
使用するSPIを`Full-Duplex Master`にする
Configulationで以下の設定をする
First Bit = LSB First
Clock Polarity = Low
Clock Phase = 2 Edge

### CSピンの設定
SPIのチップセレクト：CS(スレーブセレクト：SSとも)をGPIO_Outputで指定する。UserLavel推奨

## Library settings
controller.hにて

CONTROLLER_CS_PORT CONTROLLER_CS_PINを設定する

他のデバイスと共用で同じバスを使う場合、SHARE_SPI_BUSをdefineします。
これはSPImode(SPIの設定、の項目)が他デバイスと異なるときの対処法です

## Initialize

1. `PSControllerInit(SPI_HandleTypeDef* hspi, PS_Mode mode, PS_ModeChange modechange)`を呼ぶ。

### PSControllerInit引数詳細

- SPI_HandleTypeDef* hspi
Cubeが吐いたSPIのハンドラ構造体のポインタ
- PS_Mode mode
analogスティックを有効にするかどうか
- PS_ModeChange modechange
初期化以降にコントローラのmodeボタンからanalogスティック有効無効の変更を許可するかどうか

## Read

`PSControllerGetData()`でコントローラのデータが読めます。
注意:5ms周期未満ではコントローラのデータが読めなくなる

### PSControllerGetData()引数詳細
- uint16_t *button
ボタン情報。一ビットごとに一つのボタンが割り当てられ、押されていると1,離していると0
- analog_stick_f *left_stick
左側のスティック情報。-1.0~1.0に正規化されている。直交座標(x,y)と極座標(r,theta)の両方が読める
- analog_stick_f * right_stick
右側のスティック情報。-1.0~1.0に正規化されている。直交座標(x,y)と極座標(r,theta)の両方が読める
- vib_e small_motor
小さい方のバイブのON/OFF：`VIB_SMALL_MOTOR_ON`または`VIB_SMALL_MOTOR_OFF`を代入
- uint8_t large_motor
大きい方のバイブの振動の大きさを0~255で設定

## Code Example

- 左スティックの(x,y)と右スティックの(r,theta),の値を取得・表示
- 丸ボタンが押されたら（押されている間、ではない）`Circle`と一度表示
- 小さいバイブはON

### 想定

- 使用するのはSPI3
- modeボタンでモードチェンジさせない
- TIM6を使い10ms周期で割り込み

```c

//---some code from CubeMX---//

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>			//for printf
#include "controller.h"
/* USER CODE END Includes */

//---some code from CubeMX---//

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

#define PS_BtnChg(__BUTTON__) (!(pre_button_data&(__BUTTON__)) && (button_data&(__BUTTON__)))

/* USER CODE END PM */

//---some code from CubeMX---//

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
//for printf
int __io_putchar(int ch)
{
	HAL_UART_Transmit(&huart3, (uint8_t*) &ch, 1, 500);
	return ch;
}

//timer interrupt
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	static uint16_t pre_button_data = 0;
	static uint16_t button_data = 0;
	static analog_stick_f analog_left, analog_right;
	if(htim->Instance == TIM6){
		static int print_cnt=0;

		//Update controller data
		pre_button_data = button_data;
		//SPI communication with DUALSHOCK2
		PSControllerGetData(&button_data, &analog_left, &analog_right, VIB_SMALL_MOTOR_ON, 0);

		//print stick values
		if(print_cnt>3){ //print per 30ms
			printf("Left:(x,y)=(%f,%f)\tRight:(r,theta)=(%f,%f)\n\r", analog_left.x, analog_left.y, analog_right.r, analog_right.theta);
			print_cnt=0;
		}
		else{
			print_cnt++;
		}
		//print circle btn
		if(PS_BtnChg(CONTROLLER_CIRCLE)){
			printf("Circle\n\r");
		}
	}
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{

	//----Some Init from CubeMX----//

	/* USER CODE BEGIN 2 */
	PSControllerInit(&spi3, ANALOG_MODE, MODECHANGE_DIS);

	HAL_TIM_Base_Start_IT(&htim6);
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while(1)
	{
	/* USER CODE END WHILE */

	/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}
```