# PID Library

## Overview

PIDの式をいちいち錬成するのがめんどくさくなったのでライブラリ化。
対象ごとにパラメータを構造体でまとめて管理できる

## CubeMX setting

### 制御用タイマ割り込みの設定
タイマで割り込みをし、一定周期でPID値を更新する。エンコーダなどと共用でもよい
- ActivatedないしはInternal Clockをチェックする
- ConfigurationのPrescalerとCounterPeriodを適切に設定して、割り込み周期を設定する(個人的に10msがおすすめ)
cycle = 1/freq
freq = APBxTimerclocks / (Prescaler+1) / (CounterPeriod+1)
Prescaler, CounterPeriodの最大サイズ(bit数)に注意
- NVIC Settingsの`TIMxGlobal interrpt`をチェックする

## Initialize

1. `PID_StructTypedef`構造体を宣言
1. 構造体のkp,ki,kdを設定
1. `PID_Ctrl_Reset()`の引数に構造体をセット

#### `PID_StructTypedef`構造体内訳
- float kp;
Pゲイン
- float ki;
Iゲイン
- float kd;
Dゲイン
- float integral;
積算値：直にいじらない
- float prev_value;
前回値：直にいじらない

## Update Value

タイマ割り込み内で`PID_Ctrl()`を実行する

#### `PID_Ctrl()`引数
- PID_StructTypedef* params
設定したPID構造体
- float value
`目標値-現在値`を代入すること

## Reset PID
エンコーダの現在値を変更したときなど変化が連続でないときにPIDをリセットしないとIやDが異常動作の原因となる
そのため、プログラムでの現在値変更の際には`PID_Ctrl_Reset()`を実行し、過去の積分値及び前回値を消去する

## Code Example
モータをPID制御で目標位置(ref = 100mm)に移動する用制御するプログラム

### 想定

タイマ割り込み TIM6を使用、10ms周期

motor1：A3921_motorライブラリを参照
- A3921使用のモタドラを使っている
- PWMはTIM1Channel1
- PhaseはGPIOBのPIN1に接続, CubeMXでPORT,PINの名前を定義していない(コーディング上非推奨)
- A3921_DIR_FWが正方向だった


encoder1：encoderライブラリを参照
- タイマはTIM2
- 1024PPR, 直径60mmのタイヤ
- ENC_FW_CNTUPが正方向だった
- 初期位置は 0

ボタン(USER_Btn_GPIO_Port,USER_Btn_Pin)が押されたら、その位置を0とする

```c

//---some code from CubeMX---//

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <math.h>
#include "A3921_motor.h"
#include "encoder.h"
#include "pid.h"
/* USER CODE END Includes */

//---some code from CubeMX---//

/* USER CODE BEGIN PV */
A3921_HandleTypedef A3921_motor1;
Enc_HandleTypedef enc1;
PID_StructTypedef pid1;
/* USER CODE END PV */

//---some code from CubeMX---//

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
//timer interrupt
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	if(htim->Instance == TIM6){		//10ms timer
		float ref = 100;

		//Update encoder data
		EncoderUpdateData(&enc1);
		A3921_Run(&a3921_motor1, PID_Ctrl(&pid1, ref - __ENC_GET_POSITION(&enc1)));
	}
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{

	//----Some Init from CubeMX----//

	/* USER CODE BEGIN 2 */
	//motor1 Initialize
	A3921_motor1.timer = &htim1;
	A3921_motor1.channel = TIM_CHANNEL_1;
	A3921_motor1.direction = A3921_DIR_FW;
	A3921_motor1.phase_port = GPIOB;		//should name the port like PHASE_GPIO_Port by CubeMX
	A3921_motor1.phase_pin = GPIO_PIN_1;	//should name the pin like PHASE_Pin by CubeMX
	A3921_motor_init(&A3921_motor1);

	//encoder1 Initialize
	enc1.Init.htim = &htim2;
	enc1.Init.update_freq = 0.01f;
	enc1.Init.cnt_dir = ENC_FW_CNTUP;
	enc1.Init.value_per_pulse = M_PI * 60f / 1024;
	EncoderEnable(&enc1);

	//pid gain setting
	pid1.kp = 0.1f;
	pid1.ki = 0.001f;
	pid1.kd = 0.02f;
	PID_Ctrl_Reset(&pid1);

	//encoder1 pos set
	__ENC_SET_POSITION(&enc1, 0;

	//start ctrl timer
	HAL_TIM_Base_Start_IT(&htim6);
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while(1)
	{

		//button input
		if(HAL_GPIO_ReadPin(USER_Btn_GPIO_Port, USER_Btn_Pin)　==　GPIO_PIN_SET){
			HAL_Delay(20);	//チャタリング
			while(HAL_GPIO_ReadPin(USER_Btn_GPIO_Port, USER_Btn_Pin)　==　GPIO_PIN_SET);
			HAL_Delay(20);	//チャタリング
			//現在値を0に
			__ENC_SET_POSITION(&enc1, 0);
			//PID Reset
			PID_Ctrl_Reset(&pid1);
		}
	/* USER CODE END WHILE */

	/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}
```
