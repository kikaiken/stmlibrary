# VNH Library

## Overview

VNH(5019・3SP30等)モータドライバ用のライブラリです
Sign-magnitudeであり、Locked Anti-Phaseには対応していません
HALライブラリの構造を模倣しているため、複数のモータに対応
VNHに限らず、**PWM入力1,正逆転ピン2のモタドラに適用可能**

## CubeMX setting

### PWMピンの設定
使用するタイマのChannelを`PWM Generation CHx`にする
ConfigurationのPrescalerとCounterPeriodを適切に設定して、PWM周波数を設定する(VNHは最大1kHzとデータシートに記載)
PWMfreq = APBxTimerclocks / (Prescaler+1) / (CounterPeriod+1)
Prescaler, CounterPeriodの最大サイズ(bit数)に注意

### INA,INBピンの設定
INA,INBピンとして使うGPIOをGPIO_Outputに設定
UserLavelを設定してわかりやすくするのが好ましい

## Initialize

1. `VNH_HandleTypedef`構造体を宣言
1. 構造体の各項目を設定
1. `VNH_motor_init`の引数に構造体をセット

#### `VNH_HandleTypedef`構造体内訳
- TIM_HandleTypeDef*	timer;
使用するタイマのハンドラ
- uint32_t			channel;
使用するタイマのチャンネル
- uint8_t direction;
正回転の方向:VNH_DIR_FWかVNH_DIR_BCをセットする
- GPIO_TypeDef*	inA_port;
INAピンとして使うGPIOのPort
- uint16_t		inA_pin;
INAピンとして使うGPIOのPin
- GPIO_TypeDef*	inB_port;
INBピンとして使うGPIOのPort
- uint16_t		inB_pin;
INBピンとして使うGPIOのPin

## Run

`VNH_Run()`でモータ駆動

#### `VNH_Run()`引数
- VNH_HandleTypedef *vnh_struct
駆動させるモータのハンドル構造体
- float speed
駆動速度：-1.0 < speed < 1.0 -1.0以下及び1.0以上では速度の更新はされない（前値を保持）

## Code Example
モータを二つ段階的に回すプログラム

### 想定

motor1
- PWMはTIM1Channel1
- INAはGPIOBのPIN1に接続, CubeMXでPORT,PINの名前を定義していない(コーディング上非推奨)
- INBはGPIOBのPIN2に接続, CubeMXでPORT,PINの名前を定義していない(コーディング上非推奨)

motor2
- PWMはTIM1Channel2
- INAはGPIOBのPIN3に接続, CubeMXでPORT,PINの名前を定義していない(コーディング上非推奨)
- INBはGPIOBのPIN4に接続, CubeMXでPORT,PINの名前を定義していない(コーディング上非推奨)

```c

//---some code from CubeMX---//

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "VNH_motor.h"
/* USER CODE END Includes */

//---some code from CubeMX---//

/* USER CODE BEGIN PV */
VNH_HandleTypedef VNH_motor1;
VNH_HandleTypedef VNH_motor2;
/* USER CODE END PV */

//---some code from CubeMX---//

int main(void)
{

	//----Some Init from CubeMX----//

	/* USER CODE BEGIN 2 */
	//motor1 Initialize
	VNH_motor1.timer = &htim1;
	VNH_motor1.channel = TIM_CHANNEL_1;
	VNH_motor1.direction = VNH_DIR_FW;
	VNH_motor1.inA_port = GPIOB;		//should name the port like M1INA_GPIO_Port by CubeMX
	VNH_motor1.inA_pin = GPIO_PIN_1;	//should name the pin like M1INA_Pin by CubeMX
	VNH_motor1.inB_port = GPIOB;		//should name the port like M1INB_GPIO_Port by CubeMX
	VNH_motor1.inB_pin = GPIO_PIN_2;	//should name the pin like M1INB_Pin by CubeMX
	VNH_motor_init(&VNH_motor1);

	//motor2 Initialize
	VNH_motor2.timer = &htim1;
	VNH_motor2.channel = TIM_CHANNEL_2;
	VNH_motor2.direction = VNH_DIR_FW;
	VNH_motor2.inA_port = GPIOB;		//should name the port like M2INA_GPIO_Port by CubeMX
	VNH_motor2.inA_pin = GPIO_PIN_3;	//should name the pin like M2INA_Pin by CubeMX
	VNH_motor2.inB_port = GPIOB;		//should name the port like M2INB_GPIO_Port by CubeMX
	VNH_motor2.inB_pin = GPIO_PIN_4;	//should name the pin like M2INB_Pin by CubeMX
	VNH_motor_init(&VNH_motor2);
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while(1)
	{
		//run motor ex.
		VNH_Run(&VNH_motor1, 0.1);
		VNH_Run(&VNH_motor2, 0.2);
		HAL_Delay(2000);
		VNH_Run(&VNH_motor1, 0.5);
		VNH_Run(&VNH_motor2, 0);
		HAL_Delay(2000);
		VNH_Run(&VNH_motor1, 0);
		VNH_Run(&VNH_motor2, -0.1);
		HAL_Delay(2000);
		VNH_Run(&VNH_motor1, -0.1);
		VNH_Run(&VNH_motor2, -0.3);
		HAL_Delay(2000);
		VNH_Run(&VNH_motor1, -0.5);
		VNH_Run(&VNH_motor2, -0.1);
		HAL_Delay(2000);
		VNH_Run(&VNH_motor1, 0);
		VNH_Run(&VNH_motor2, 0);
		HAL_Delay(2000);
	/* USER CODE END WHILE */

	/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}
```