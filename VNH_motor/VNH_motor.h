/*
 * VNH_motor.h
 *
 *  Created on: 2019/11/18
 *      Author: nabeya11
 */

#ifndef VNH_MOTOR_H_
#define VNH_MOTOR_H_

#include "main.h"

#define VNH_DIR_FW	0
#define VNH_DIR_BC	1

typedef struct{
	TIM_HandleTypeDef*	timer;	//timer構造体
	uint32_t			channel;	//timerチャンネル
	uint8_t direction;	//正回転の方向
	GPIO_TypeDef*	inA_port;	//GPIO_Port
	uint16_t		inA_pin;	//GPIO_Pin
	GPIO_TypeDef*	inB_port;	//GPIO_Port
	uint16_t		inB_pin;	//GPIO_Pin
}VNH_HandleTypedef;

void VNH_motor_init(VNH_HandleTypedef *vnh_struct);
void VNH_Run(VNH_HandleTypedef *vnh_struct, float speed);

#endif /* VNH_MOTOR_H_ */
