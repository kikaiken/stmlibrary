/*
 * VNH_motor.c
 *
 *  Created on: 2019/11/18
 *      Author: nabeya11
 */

#include "VNH_motor.h"
#include <math.h>

uint32_t mspeed_32;

void VNH_motor_init(VNH_HandleTypedef *vnh_struct){
	__HAL_TIM_SET_COMPARE(vnh_struct->timer, vnh_struct->channel, 0);
	HAL_TIM_PWM_Start(vnh_struct->timer, vnh_struct->channel);
}

void VNH_Run(VNH_HandleTypedef *vnh_struct, float speed){
	if(fabsf(speed) <= 1.0f){
		//Forward / Reverse
		if((speed >= 0.0f) ^ vnh_struct->direction){
			HAL_GPIO_WritePin(vnh_struct->inA_port, vnh_struct->inA_pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(vnh_struct->inB_port, vnh_struct->inB_pin, GPIO_PIN_RESET);
		}
		else{
			HAL_GPIO_WritePin(vnh_struct->inA_port, vnh_struct->inA_pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(vnh_struct->inB_port, vnh_struct->inB_pin, GPIO_PIN_SET);
		}
		mspeed_32=(uint32_t)fabsf(speed*__HAL_TIM_GET_AUTORELOAD(vnh_struct->timer));
		__HAL_TIM_SET_COMPARE(vnh_struct->timer, vnh_struct->channel, mspeed_32);
	}
}
