/*
 * buzzer.hpp
 *
 *  Created on: 2019/11/18
 *      Author: nabeya11
 */

#ifndef BUZZER_H_
#define BUZZER_H_
#ifdef __cplusplus
extern "C" {
#endif


#include "main.h"

/*-----------------------------------------------
 * Buzzer
 ----------------------------------------------*/
//ブザータイマーハンドラ
#define BUZZER_TIM_HANDLER		htim11

//ブザータイマーチャンネル
#define BUZZER_TIM_CHANNEL		TIM_CHANNEL_1

//ブザータイマー周波数（分周後）
#define BUZZER_TIM_FREQ			1000000.0f

//ブザー時間タイマ
#define TIM_MELODY_HANDLER		htim5

//音階
enum {
	BUZZER_NO  =    0,
	BUZZER_A4  =  440,
	BUZZER_B4  =  494,
	BUZZER_C5  =  524,
	BUZZER_CM5 =  554,
	BUZZER_D5  =  588,
	BUZZER_Em5 =  622,
	BUZZER_E5  =  660,
	BUZZER_F5  =  699,
	BUZZER_FM5 =  740,
	BUZZER_G5  =  784,
	BUZZER_GM5 =  831,
	BUZZER_Am5 =  831,
	BUZZER_A5  =  880,
	BUZZER_AM5 =  932,
	BUZZER_Bm5 =  932,
	BUZZER_B5  =  988,
	BUZZER_C6  = 1047,
	BUZZER_CM6 = 1109,
	BUZZER_Dm6 = 1109,
	BUZZER_D6  = 1175,
	BUZZER_Em6 = 1245,
	BUZZER_E6  = 1319,
	BUZZER_F6  = 1397,
	BUZZER_FM6 = 1480,
	BUZZER_G6  = 1568,
	BUZZER_GM6 = 1661,
	BUZZER_Am6 = 1661,
	BUZZER_A6  = 1760,
	BUZZER_AM6 = 1865,
	BUZZER_Bm6 = 1865,
	BUZZER_B6  = 1976,
	BUZZER_C7  = 2093,
	BUZZER_CM7 = 2217,
	BUZZER_Dm7 = 2217,
	BUZZER_D7  = 2349,
	BUZZER_Em7 = 2489,
	BUZZER_E7  = 2637,
	BUZZER_F7  = 2794,
	BUZZER_FM7 = 2960,
	BUZZER_G7  = 3136,
	BUZZER_GM7 = 3322,
	BUZZER_Am7 = 3322,
	BUZZER_A7  = 3520,
	BUZZER_Bm7 = 3729,
	BUZZER_B7  = 3951,
	BUZZER_C8  = 4186,
};

enum {
	ONE_2 = 1000,
	ONE_4 = ONE_2/2,
	ONE_8 = ONE_4/2,
	ONE_16 = ONE_8/2,
	ONE_32 = ONE_16/2
};

//プロトタイプ宣言
void BuzzerBeep(int16_t, int32_t);
void BuzzerOn(int16_t);
void BuzzerOff(void);
void Melody_Start(uint8_t value);
uint8_t Melody_Update(void);
void Melody(uint8_t value);
void Arpeggio(uint8_t value);

#ifdef __cplusplus
}
#endif
#endif /* BUZZER_H_ */
