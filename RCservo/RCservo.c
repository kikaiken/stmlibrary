/*
 * RCservo.c
 *
 *  Created on: 2019/08/18
 *      Author: nabeya11
 */

#include "RCservo.h"
#include "light_math.h"

void RCservo_Init(struct_servo* servo_h){
	assert_param(servo_h->htim);
	servo_h->center_pos = __HAL_TIM_GET_AUTORELOAD(servo_h->htim) * 15 / 200;//center:1.5ms, freq:20ms
	HAL_TIM_PWM_Start(servo_h->htim, servo_h->channel);
}

//deg:-90~90
void RCservo_DegCtrl(struct_servo* servo_h, float deg){
	__HAL_TIM_SET_COMPARE(servo_h->htim, servo_h->channel, servo_h->center_pos + (int16_t)(__HAL_TIM_GET_AUTORELOAD(servo_h->htim)*deg*5*1.4f/90 / 200));
}

//rad:-Pi/2~Pi/2
void RCservo_RadCtrl(struct_servo* servo_h, float rad){
	RCservo_DegCtrl(servo_h, RAD2DEG(rad));
}
