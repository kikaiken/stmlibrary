# STM Library

各デバイスをSTM32で使うライブラリ群
それぞれのライブラリの仕様はそのフォルダ内のReadmeに記載
Font Code:UFT-8

## How to use

使用したいライブラリをフォルダから出して、
.hを使用先プロジェクトのIncに、.cをSrcディレクトリに配置する。

## Contents

(未)：未完成

- A3921motor
ゲートドライバA3921を使用したモータドライバ向けのライブラリ
- AQM0802A
(未)題のLCDをi2cで使うためのライブラリ
- buzzer
(未)PWMでブザーを使ったライブラリ。中に
・単音ブザー
・15種類のメロディ
が含まれる
- controller
PSコントローラ(dualshock)をSPIで使用するためのライブラリ
- dynamixel
（未）TTL式のdynamixel
- encoder
STMのエンコーダモードを利用して、実際の距離・速度、角度・角速度を算出するためのライブラリ
- fxas21002
題のジャイロセンサを利用するためのライブラリ
- lightmath
簡単な計算アルゴリズムを含んだライブラリ
- lsm6ds3
- mpu9250
題のIMU（加速度・ジャイロ）をSPIで使うためのライブラリ
- pid
PID制御のためのライブラリ
- RCservo
(未)ラジコン用サーボを角度入力で使用するためのライブラリ
- so1602a
(未)題のOLEDディスプレイをI2Cで使用するためのライブラリ
- VNHmotor
モータドライバVNH5019Aを使用するためのライブラリ