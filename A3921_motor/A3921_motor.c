/*
 * A3921_motor.c
 *
 *  Created on: Nov 24, 2019
 *      Author: nabeya11
 */

#include "A3921_motor.h"
#include <math.h>

void A3921_motor_init(A3921_HandleTypedef *a3921_struct){
	HAL_TIM_PWM_Start(a3921_struct->timer, a3921_struct->channel);
}

void A3921_Run(A3921_HandleTypedef *a3921_struct, float speed){
	if(fabsf(speed) <= 1.0f){
		//Forward / Reverse
		if((speed >= 0.0f)^a3921_struct->direction){
			HAL_GPIO_WritePin(a3921_struct->phase_port, a3921_struct->phase_pin, GPIO_PIN_SET);
		}
		else{
			HAL_GPIO_WritePin(a3921_struct->phase_port, a3921_struct->phase_pin, GPIO_PIN_RESET);
		}
		uint32_t mspeed_32=fabsf(speed*__HAL_TIM_GET_AUTORELOAD(a3921_struct->timer));
		__HAL_TIM_SET_COMPARE(a3921_struct->timer, a3921_struct->channel, mspeed_32);
	}
}
