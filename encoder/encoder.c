/*
 * encoder.c
 *
 *  Created on: 2020/03/09
 *      Author: nabeya11
 */


#include "encoder.h"
#include <stdio.h>
#include <math.h>

/*
 * エンコーダ値からロボットの速さを求める。オドメトリも求める。
 * @param
 * @return
 * @note	update_freq(s)毎に呼び出すこと。
 */
void EncoderUpdateData(Enc_HandleTypedef *enc_state) {
	int32_t present_cnt = __HAL_TIM_GET_COUNTER(enc_state->Init.htim);
	int32_t diff_cnt;
	volatile float diff;

	//差分と向き
	diff_cnt = enc_state->Init.cnt_dir * (present_cnt - enc_state->prev_cnt);

	//a.物理量に変換
	diff = (float)diff_cnt * enc_state->Init.value_per_pulse / 4;

	//b.速度の計算
	enc_state->vel = diff / enc_state->Init.update_freq;

	//c.位置の計算
	enc_state->pos += diff;

	//d.カウント値保存
	enc_state->prev_cnt = present_cnt;
	//e.オーバーフローしそうになったらカウント値リセット
	if ((present_cnt > (ENC_CNT_PERIOD-ENC_CNT_MARGIN)) || (present_cnt < ENC_CNT_MARGIN)) {
		__HAL_TIM_SET_COUNTER(enc_state->Init.htim, ENC_CNT_RESET);
		enc_state->prev_cnt = ENC_CNT_RESET;
	}
}

/*
 * エンコーダカウントスタート
 * @param
 * @return
 */
void EncoderEnable(Enc_HandleTypedef* enc_state) {
	printf("Encoder : Enable\n\r");
	HAL_TIM_Encoder_Start(enc_state->Init.htim, TIM_CHANNEL_ALL);
	__HAL_TIM_SET_COUNTER(enc_state->Init.htim, ENC_CNT_RESET);
	enc_state->prev_cnt = ENC_CNT_RESET;
}

/*
 * エンコーダカウントストップ
 * @param
 * @return
 */
void EncoderDisable(Enc_HandleTypedef* enc_state) {
	printf("Encoder : Disable\n\r");
	HAL_TIM_Encoder_Stop(enc_state->Init.htim, TIM_CHANNEL_ALL);
}
