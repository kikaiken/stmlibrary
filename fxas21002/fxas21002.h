#ifndef FXAS21002_H_INCLUDED
#define FXAS21002_H_INCLUDED

typedef struct {
    float pitch, roll, yaw;
} fxas21002_data_t;

typedef enum {
    FXAS21002_REGISTER_STATUS       = 0x00, /**< 0x00 */
    FXAS21002_REGISTER_OUT_X_MSB    = 0x01, /**< 0x01 */
    FXAS21002_REGISTER_OUT_X_LSB    = 0x02, /**< 0x02 */
    FXAS21002_REGISTER_OUT_Y_MSB    = 0x03, /**< 0x03 */
    FXAS21002_REGISTER_OUT_Y_LSB    = 0x04, /**< 0x04 */
    FXAS21002_REGISTER_OUT_Z_MSB    = 0x05, /**< 0x05 */
    FXAS21002_REGISTER_OUT_Z_LSB    = 0x06, /**< 0x06 */
    FXAS21002_REGISTER_WHO_AM_I     = 0x0C, /**< 0x0C (default value = 0b11010111, read only) */
    FXAS21002_REGISTER_CTRL_REG0    = 0x0D, /**< 0x0D (default value = 0b00000000, read/write) */
    FXAS21002_REGISTER_CTRL_REG1    = 0x13, /**< 0x13 (default value = 0b00000000, read/write) */
    FXAS21002_REGISTER_CTRL_REG2    = 0x14, /**< 0x14 (default value = 0b00000000, read/write) */
} fxas2100_registers_t;

typedef enum {
    FXAS21002_RANGE_250DPS = 250,
    FXAS21002_RANGE_500DPS = 500,
    FXAS21002_RANGE_1000DPS = 1000,
    FXAS21002_RANGE_2000DPS = 2000,
} fxas21002_range_t;

void fxas21002Init(I2C_HandleTypeDef *phi2c, fxas21002_range_t range);
void fxas21002GetAngVel(fxas21002_data_t *ret);
float fxas21002GetYawAngVel();

#endif
